<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

use RKFL\Api\Client\Rocketfuel;

/**
 * Class for Syntactic sugar to reduce verbiage
 */
final class Service {

	private Rocketfuel $_sdk;
	/**
	 * Constructor for Authorization
	 *
	 * @param Rocketfuel $sdk Rocketfuel instance
	 */
	public function __construct( Rocketfuel $sdk ) {
		$this->_sdk = $sdk;
	}
	/**
	 * Get content from response
	 *
	 * @param array $payload
	 *
	 * @return array
	 */
	public function getUUID( array $payload ): array {
		try {
			$result           = $this->_sdk->authorization()->login();
			$hostedPageResult = $this->_sdk->hostedPage()->create(
				array(
					'access' => $result['result']['access'],
					'body'   => $payload,
				)
			);
		} catch ( \Throwable $th ) {
			throw $th;
		}

		return $hostedPageResult;
	}
}
