<?php

declare(strict_types=1);

namespace RKFL\Api\Client\HttpClient\Message;

use Psr\Http\Message\ResponseInterface;

/**
 * Class for parsing response
 */
final class ResponseMediator
{
    /**
     * Get content from response
     * 
     * @param ResponseInterface $response
     * 
     * @return array
     */
    public static function getContent(ResponseInterface $response): array
    {
      
        try {
            $result= json_decode($response->getBody()->getContents(), true);

            if(is_null($result)){
                return [
                    'error' => true,
                    'message' => 'No content was returned'
                ];
            }
            return $result;
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}
