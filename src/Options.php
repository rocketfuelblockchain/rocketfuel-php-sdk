<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;

final class Options {

	private array $options;

	public function __construct( array $options = array() ) {
		// $this->options = $options;
		$resolver = new OptionsResolver();

		if ( null !== $options['environment'] ) {
			$baseUrl = $this->_rkflBaseUrl( $options['environment'] );
			$options = array_merge( $options, array( 'uri' => $baseUrl ) );
		}

		$this->_configureOptions( $resolver );

		$this->options = $resolver->resolve( $options );
	}
	/**
	 * Get RKFL Base Url
	 *
	 * @param string $environment
	 *
	 * @return string
	 */
	private function _rkflBaseUrl( string $environment ): string {
		$environment_data = array(
			'prod'    => 'https://app.rocketfuelblockchain.com/api',
			'dev'     => 'http://localhost:3001/api',
			'sandbox' => 'https://app-sandbox.rocketfuelblockchain.com/api',
			'qa'      => 'https://qa-app.rocketdemo.net/api',
		);

		return $environment_data[ $environment ] ?? 'https://app.rocketfuelblockchain.com/api';
	}

	/**
	 * Configure Client Options
	 *
	 * @return void
	 */
	private function _configureOptions( OptionsResolver $resolver ): void {
		$clientBuilder = new ClientBuilder();

		$clientBuilder->addPlugin(
			new HeaderDefaultsPlugin(
				array(
					'Accept' => 'application/json',
				)
			)
		);
		// TODO: Set default merchant details from environment variables
		$resolver->setDefaults(
			array(
				'client_builder'      => new ClientBuilder(),
				'uri_factory'         => Psr17FactoryDiscovery::findUriFactory(),
				'uri'                 => 'https://app.rocketfuelblockchain.com/api',
				'environment'         => Constants::DEFAULT_ENVIRONMENT,
				'password'            => '',
				'email'               => '',
				'merchant_id'         => '',
				'merchant_public_key' => '',
				'client_id'           => '',
				'client_secret'       => '',
				'version'             => Constants::DEFAULT_VERSION,
			)
		);

		$resolver->setAllowedTypes( 'uri', 'string' );
		$resolver->setAllowedTypes( 'email', 'string' );
		$resolver->setAllowedTypes( 'password', 'string' );
		$resolver->setAllowedTypes( 'environment', 'string' );
		$resolver->setAllowedTypes( 'merchant_id', 'string' );
		$resolver->setAllowedTypes( 'merchant_public_key', 'string' );
		$resolver->setAllowedTypes( 'version', 'string' );
		$resolver->setAllowedTypes( 'client_id', 'string' );
		$resolver->setAllowedTypes( 'client_secret', 'string' );

		$resolver->setAllowedTypes( 'client_builder', ClientBuilder::class );

		$resolver->setAllowedTypes( 'uri_factory', UriFactoryInterface::class );
	}

	/**
	 * Get builder method
	 *
	 * @return ClientBuilder
	 */
	public function getClientBuilder(): ClientBuilder {
		return $this->options['client_builder'];
	}
	/**
	 * Get Merchant Public key
	 *
	 * @return string
	 */
	public function getMerchantPublicKey() {
		return $this->options['merchant_public_key'];
	}
	/**
	 * Get Merchant Credentials
	 *
	 * @return array
	 */
	public function getMerchantCred() {
		return array(
			'password' => $this->options['password'],
			'email'    => $this->options['email'],
		);
	}
	/**
	 * Get Merchant Id
	 *
	 * @return
	 */
	public function getMerchantId() {
		return $this->options['merchant_id'];
	}

	public function getUriFactory(): UriFactoryInterface {
		return $this->options['uri_factory'];
	}

	public function getVersion(): string {
		return $this->options['version'] ?? 'v1';
	}
	public function getClientSecret(): string {
		return $this->options['client_secret'];
	}
	public function getClientId(): string {
		return $this->options['client_id'];
	}

	public function getUri(): UriInterface {
		return $this->getUriFactory()->createUri( $this->options['uri'] ?? 'https://app.rocketfuelblockchain.com/api' );
	}
}
