<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

/**
 * Class for Various helper methods
 */
final class Helpers
{


    private static function getPublicKey()
    {
        $pubKeyPath = dirname(__FILE__) . '/key/rf.pub';


        if (!file_exists($pubKeyPath)) {
            return false;
        }
        return file_get_contents($pubKeyPath);
    }
    /**
     * Cryptojs compatible encryption
     */
    public static  function encrypt(string $toEncrypt, string $secret)
    {
        $salt = openssl_random_pseudo_bytes(8);

        $salted = $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx . $secret . $salt, true);
            $salted .= $dx;
        }

        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);

        // encrypt with PKCS7 padding
        return base64_encode('Salted__' . $salt . openssl_encrypt($toEncrypt . '', 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv));
    }
    /**
     * Encrypt String With Key
     * 
     * @param string $toEncrypt
     * @param string|null $cert
     * 
     * @return string
     */
    public static function getEncrypted(string $toEncrypt, string $cert = null): string
    {
        $out = '';

        $cert = is_null($cert) ? self::getPublicKey() : $cert;

        $publicKey = openssl_pkey_get_public($cert);

        $keyLength = openssl_pkey_get_details($publicKey);

        $partLen = $keyLength['bits'] / 8 - 11;
        $parts = str_split($toEncrypt, $partLen);

        foreach ($parts as $part) {
            $encryptedTemp = '';
            openssl_public_encrypt($part, $encryptedTemp, $publicKey, OPENSSL_PKCS1_OAEP_PADDING);
            $out .=  $encryptedTemp;
        }

        return base64_encode($out);
    }

    /**
     * Verify if Webhook is from Rocketfuel
     * 
     * @param string $data - Data value from Webhook payload
     * @param string $signature - Signature value from Webhook payload
     * 
     * @return bool
     */
    public function verifyWebhook(string $data, string $signature): bool
    {

        $signatureBuffer = base64_decode($signature);

        return (1 === openssl_verify($data, $signatureBuffer, self::getPublicKey(), OPENSSL_ALGO_SHA256));
    }
}
