<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

 

/**
 * Class for Various helper methods
 */
final class Constants
{
    const DEFAULT_VERSION= 'v1';
    const DEFAULT_ENVIRONMENT= 'prod';
    
}