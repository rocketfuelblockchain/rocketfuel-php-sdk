<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

use RKFL\Api\Client\Endpoint\HostedPage;
use RKFL\Api\Client\Endpoint\Authorization;
use RKFL\Api\Client\Endpoint\Update;
use RKFL\Api\Client\Service;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use RKFL\Api\Client\Helpers;

/**
 * Class for Rocketfuel Instance
 *
 * @category Rocketfuel
 * @package  RKFL
 * @author   Rocketfuel Team <dev@rocketfuelblockchain.com>
 * @link     https://rocketfuelblockchain.com
 * @license  MIT
 */
final class Rocketfuel {

	private ClientBuilder $_clientBuilder;
	private array $_cred;
	private string $_merchantId;
	private string $_merchantPublicKey;
	private string $_version;
	private string $_clientId;
	private string $_clientSecret;

	/**
	 * Construct Rocketfuel
	 *
	 * @params Options $options Options for configuration
	 */
	public function __construct( Options $options = null ) {
		$options = $options ?? new Options();

		$this->_clientBuilder = $options->getClientBuilder();

		$this->_cred              = $options->getMerchantCred();
		$this->_merchantId        = $options->getMerchantId();
		$this->_merchantPublicKey = $options->getMerchantPublicKey();
		$this->_version           = $options->getVersion();
		$this->_clientId          = $options->getClientId();
		$this->_clientSecret      = $options->getClientSecret();

		$this->_clientBuilder->addPlugin( new BaseUriPlugin( $options->getUri() ) );

		$this->_clientBuilder->addPlugin(
			new HeaderDefaultsPlugin(
				array(
					'User-Agent'   => 'RKFL PHP SDK',
					'Content-Type' => 'application/json',
					'Accept'       => 'application/json',
				)
			)
		);
	}



	/**
	 * HostedPage Endpoint instance
	 *
	 * @return HostedPage
	 */
	public function hostedPage(): HostedPage {
		return new Endpoint\HostedPage( $this );
	}

	/**
	 * Update Endpoint instance
	 *
	 * @return Update
	 */
	public function update(): Update {
		return new Endpoint\Update( $this );
	}
	/**
	 * HostedPage method instance
	 *
	 * @return Service
	 */
	public function service(): Service {
		return new Service( $this );
	}
	/**
	 * HostedPage method instance
	 *
	 * @return Authorization
	 */
	public function authorization(): Authorization {
		return new Endpoint\Authorization( $this );
	}

	/**
	 * Get the HTTP Client
	 *
	 * @return HttpMethodsClientInterface
	 */
	public function getHttpClient(): HttpMethodsClientInterface {
		return $this->_clientBuilder->getHttpClient();
	}
	/**
	 * Get the Merchant ID
	 *
	 * @return string
	 */
	public function getMerchantId(): string {
		return $this->_merchantId;
	}
	/**
	 * Get Merchant Public Key
	 *
	 * @return string
	 */
	public function getMerchantPublicKey(): string {
		return $this->_merchantPublicKey;
	}
	/**
	 * Get ClientId
	 *
	 * @return string
	 */
	public function getClientId(): string {
		return $this->_clientId;
	}
	/**
	 * Get ClientId
	 *
	 * @return string
	 */
	public function getClientSecret(): string {
		return $this->_clientSecret;
	}

	/**
	 * Get the Merchant Cred. This is an array of merchant email and password
	 *
	 * @return array
	 */
	public function getMerchantCred(): array {
		return $this->getVersion() === Constants::DEFAULT_VERSION ? array(
			'clientId'         => $this->getClientId(),
			'encryptedPayload' => Helpers::encrypt(
				json_encode(
					array(
						'merchantId' => $this->getMerchantId(),
						'totp'       => '',
					)
				),
				$this->getClientSecret()
			),
		) : $this->_cred;
	}
	/**
	 * Get the Merchant Cred. This is an array of merchant email and password
	 *
	 * @return array
	 */
	public function getVersion(): string {
		return $this->_version;
	}

	/**
	 * Get the Merchant Cred. This is an array of merchant email and password
	 *
	 * @return Helpers
	 */
	public function helpers(): Helpers {
		return new Helpers();
	}
}
