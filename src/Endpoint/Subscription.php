<?php

declare(strict_types=1);

namespace RKFL\Api\Client\Endpoint;

use RKFL\Api\Client\Helpers;
use RKFL\Api\Client\HttpClient\Message\ResponseMediator;
use RKFL\Api\Client\Rocketfuel;

/**
 * Class for Subscription Instance
 * 
 * @category Subscription
 * @package  RKFL
 * @author   Rocketfuel Team <dev@rocketfuelblockchain.com>
 * @license  MIT https://MIT.com
 * @link     https://rocketfuelblockchain.com
 */
final class Subscription
{
    private Rocketfuel $_sdk;
    private string $_baseResource = '/subscription';

    /**
     * Constructor for Subscription
     * 
     * @param Rocketfuel $sdk Rocketfuel instance
     */
    public function __construct(Rocketfuel $sdk)
    {
        $this->_sdk = $sdk;
    }

    /**
     * Cancel Subscription
     * 
     * @param string $subscriptionId
     * 
     * @return array
     */
    public function cancel(string $subscriptionId): array
    {
        $payload = Helpers::getEncrypted(
            json_encode(
                ['subscriptionId' => $subscriptionId]
            ),
            $this->_sdk->getMerchantPublicKey()
        );

        $merchantId = base64_encode($this->_sdk->getMerchantId());

        $body = [
            'merchantId' => $merchantId,
            'merchantAuth' => $payload,
            'subscriptionId' => $subscriptionId
        ];

        return ResponseMediator::getContent(
            $this->_sdk->getHttpClient()->post($this->_baseResource . '/cancel', [], json_encode($body))
        );
    }
    /**
     * Manually Debit Subscription(s)
     * 
     * @param string $orderId The orderId connected to the subscriptions
     * @param array $subscriptionData Subscription Data is an array of arrays with values subscriptionId, amount and currency. 
     * array(
     *  [subscriptionId=>'',amount=>123,currency=>''],
     *  ...array
     * )
     * 
     * @return array
     */
    public function debit(string $orderId, array $subscriptionData): array
    {

        $merchantAuth = Helpers::getEncrypted(
            json_encode(
                ['merchantId' => $this->_sdk->getMerchantId()]
            ),
            $this->_sdk->getMerchantPublicKey()
        );

        $merchantId = base64_encode($this->_sdk->getMerchantId());

        $body = [
            'merchantId' => $merchantId,
            'merchantAuth' => $merchantAuth,
            "orderId" => $orderId,
            'items' => $subscriptionData
        ];

        return ResponseMediator::getContent(
            $this->_sdk->getHttpClient()->post($this->_baseResource . '/debit', [], json_encode($body))
        );
    }
}
