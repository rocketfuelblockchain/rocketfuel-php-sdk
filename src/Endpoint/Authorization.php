<?php

declare(strict_types=1);

namespace RKFL\Api\Client\Endpoint;

use RKFL\Api\Client\Constants;
use RKFL\Api\Client\HttpClient\Message\ResponseMediator;
use RKFL\Api\Client\Rocketfuel;

/**
 * Class for Authorization Instance
 *
 * @category Authorization
 * @package  RKFL
 * @author   Rocketfuel Team <dev@rocketfuelblockchain.com>
 * @license  MIT https://MIT.com
 * @link     https://rocketfuelblockchain.com
 */
final class Authorization {

	private Rocketfuel $_sdk;
	private string $_baseResource = '/auth';

	/**
	 * Constructor for Authorization
	 *
	 * @param Rocketfuel $sdk Rocketfuel instance
	 */
	public function __construct( Rocketfuel $sdk ) {
		$this->_sdk = $sdk;
	}

	/**
	 * Get Access token
	 *
	 * @return array
	 */
	public function login(): array {

		if ( $this->_sdk->getVersion() === Constants::DEFAULT_VERSION ) {
			return ResponseMediator::getContent(
				$this->_sdk->getHttpClient()->post( $this->_baseResource . '/generate-auth-token', array(), json_encode( $this->_sdk->getMerchantCred() ) )
			);

		}

		return ResponseMediator::getContent(
			$this->_sdk->getHttpClient()->post( $this->_baseResource . '/login', array(), json_encode( $this->_sdk->getMerchantCred() ) )
		);
	}
}
