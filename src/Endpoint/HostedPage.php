<?php

declare(strict_types=1);

namespace RKFL\Api\Client\Endpoint;

use RKFL\Api\Client\HttpClient\Message\ResponseMediator;
use RKFL\Api\Client\Rocketfuel;

/**
 * Class for Hostedpage Endpoint
 */
final class HostedPage
{
    private Rocketfuel $_sdk;
    private string $_baseResource = '/hosted-page';

    /**
     * Constructor for Hostedpage
     */
    public function __construct(Rocketfuel $sdk)
    {
        $this->_sdk = $sdk;
    }

    /**
     * Get UUID for cart
     * 
     * @param array $post
     * 
     * @return array
     */
    public function create(array $post): array
    {
     

        $accessToken = $post['access'];

        $body = array_merge(
            $post['body'],['merchant_id'=> $this->_sdk->getMerchantId() ]
        );
        
        return ResponseMediator::getContent(
            $this->_sdk->getHttpClient()->post($this->_baseResource,['authorization' => "Bearer ". $accessToken],
            json_encode($body ))
        );
    }
}