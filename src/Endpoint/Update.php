<?php

declare(strict_types=1);

namespace RKFL\Api\Client\Endpoint;

use RKFL\Api\Client\Helpers;
use RKFL\Api\Client\HttpClient\Message\ResponseMediator;
use RKFL\Api\Client\Rocketfuel;

/**
 * Class for Authorization Endpoint
 *  
 * @category Authorization
 * @package  RKFL
 * @author   Rocketfuel Team <dev@rocketfuelblockchain.com>
 * @license  MIT https://MIT.com
 * @link     https://rocketfuelclockchain.com
 */
final class Update
{
    private Rocketfuel $_sdk;
    private string $_baseResource = '/update';

    /**
     * Constructor for Update
     * 
     * @param Rocketfuel $sdk Rocketfuel instance
     */
    public function __construct(Rocketfuel $sdk)
    {
        $this->_sdk = $sdk;
    }

    /**
     * Swap Temporary OrderId for New Order Id 
     * @param string $temporaryOrderId
     * @param string $newOrderId
     * 
     * @return array
     */
    public function order(string $temporaryOrderId, string $newOrderId): array
    {
        if (empty(trim($temporaryOrderId)) || empty(trim($newOrderId))) {
            return [
                'error' => true,
                'message' => 'Empty values not allowed'
            ];
        }

        $toEncrypt = json_encode(
            [
                'tempOrderId' => $temporaryOrderId,
                'newOrderId' =>  $newOrderId
            ]
        );

        $orderPayload = Helpers::getEncrypted($toEncrypt, $this->_sdk->getMerchantPublicKey());

        $merchantId = base64_encode($this->_sdk->getMerchantId());

        $body = json_encode(
            [
                'merchantAuth' => $orderPayload,
                'merchantId' =>   $merchantId
            ]
        );

        $result =  ResponseMediator::getContent(
            $this->_sdk->getHttpClient()->post(
                $this->_baseResource + '/orderId',
                [],
                $body
            )
        );

        return $result;
    }
}
