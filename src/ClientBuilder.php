<?php

declare(strict_types=1);

namespace RKFL\Api\Client;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin;
use Http\Client\Common\PluginClientFactory;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

/**
 * Class for Client Builder
 * @category Clientbuilder
 * @package rkfl
 * @license MIT
 */
final class ClientBuilder
{
    private ClientInterface $_httpClient;
    
    private RequestFactoryInterface $requestFactoryInterface;
    
    private StreamFactoryInterface $streamFactoryInterface;
    
    private array $_plugins = [];
    
    public function __construct(
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactoryInterface = null,
        StreamFactoryInterface $streamFactoryInterface = null
    ) {
        $this->_httpClient = $httpClient ?: HttpClientDiscovery::find();
        $this->requestFactoryInterface = $requestFactoryInterface ?: Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactoryInterface = $streamFactoryInterface ?: Psr17FactoryDiscovery::findStreamFactory();
    }
    
    public function addPlugin(Plugin $plugin): void
    {
        $this->_plugins[] = $plugin;   
    }
    
    public function getHttpClient(): HttpMethodsClientInterface
    {
        $pluginClient = (new PluginClientFactory())->createClient($this->_httpClient, $this->_plugins);
        
        return new HttpMethodsClient(
            $pluginClient,
            $this->requestFactoryInterface,
            $this->streamFactoryInterface
        );
    }
}