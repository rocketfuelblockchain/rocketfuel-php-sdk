# PHP library for the Rocketfuel API.

This is a PHP client library for accessing the Rocketfuel API.

Where possible, the services available on the client groups the API into logical chunks and correspond to the structure of the Rocketfuel API documentation at https://docs.rocketfuelblockchain.com/developer-guides/api-reference


# Installation
##via composer

    composer require rkfl/rocketfuel-php-sdk 

##Manual install
composer will install all dependencies

    git clone https://bitbucket.org/rocketfuelblockchain/rocketfuel-php-sdk.git
    cd rocketfuel-php-sdk
    composer install

##Generate Invoice link
``` PHP

use Api\Client\Options;
use Api\Client\Rocketfuel;

require_once PATH_TO_VENDOR . '/vendor/autoload.php';
$options = new Options(
    [
        'environment' => 'sandbox', //or prod
        'merchant_id' => 'MERCHANT_ID',
        'merchant_public_key' => "PUBLIC_KEY",
        'client_id'=>"CLIENT_ID",
        'client_secret'=>"CLIENT_SECRET",
    ]
);
$payload = [
    'amount' => '100',
    'cart' => [
        [
            'id' => '1',
            'name' => 'test',
            'price' => '100',
            'quantity' => '1'
        ]
    ],
    'currency' => 'USD',
    'order' => 'TEST'
];

$sdk = new Rocketfuel($options);

$serviceResult = $sdk->service()->getUUID($payload);


```
## Verify Webhook
``` PHP
$serviceResult = $sdk->helpers()->verifyWebhook($postData->data);
```

## public key should be in PEM format, copy it from your RocketFuel merchant account

# Setup

* "Merchant ID" (provided in RocketFuel merchant UI for registered merchants).
* "callback URL" use your own url for callback from RocketFuel service.
* In the RocketFuel UI for registered merchants click "Edit" in the bottom left corner. A window will pop up and paste callback URL.

## TODO
- [ ] Tests


