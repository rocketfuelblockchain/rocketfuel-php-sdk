<?php

/**
 * ClientBuilder
 *
 * @category Example
 *
 * @package Rocketfuel
 * @author  Rocketfuel Team <dev@rocketfuelblockchain.com>
 * @link    https://rocketfuelblockhain.com
 * @license MIT
 */

use RKFL\Api\Client\Options as options;
use RKFL\Api\Client\Rocketfuel;

require_once dirname( __DIR__, 1 ) . '/vendor/autoload.php';

// $options = new Options(
// [
// 'environment' => 'sandbox',
// 'merchant_id' => 'MERCHANT_ID',
// 'password' => 'PASSWORD',
// 'email' => 'EMAIL',
// 'merchant_public_key' => "PUBLIC_KEY"
// ]
// );

$options = new Options( json_decode( file_get_contents( __DIR__ . '/options.sample-v1.json' ), true ) );

$payload = array(
	'amount'   => '100',
	'cart'     => array(
		array(
			'id'       => '1',
			'name'     => 'test',
			'price'    => '100',
			'quantity' => '1',
		),
	),
	'currency' => 'USD',
	'order'    => (string) time(),
);

$rkfl = new Rocketfuel( $options );

$serviceResult = $rkfl->service()->getUUID( $payload );

// $serviceResult = $rkfl->helpers()->verifyWebhook('{"amount":"3","conversionRate":{"fiatCurrency":"USD","rate":1},"cryptoAmount":"3","cryptoCurrency":"USD","currency":"USD","offerId":"d5d1f610763cb176a6b71f92f47d3adb","paymentStatus":"0","receivedAmount":"0","referenceId":"e1bb3a33-6723-4482-a339-4b9a860c3945","status":true,"transactionId":"31f67bdc-4b2b-4cbc-8547-909ad87d0f40"}', 'Tc/87MzGVoTO5qrZ/ZkzEzkfLQzn9pG8uQLAKFleUHxh7Y2eAiJ5qc2TBO3hG3lp7kispWaItzeiiSIHio9IM8z6RcFLnm1y/SuOBx48w/cYlDRR5LnrLz1m+5amPJ2cSRf3g8QkO4i7K81q1LrEi5YftNUHtVANKa9iQBZlboYW2kJQwqS0xb3L3n6Mkh30ABXuPtoO6pNowUoDLtIkoUbljbeeXkz1zrI+E4q8n9Qb4zcdX+NA5aa+AdgJdlPx1B6p0m2X1xk3ExecJ9omWYM8MUc+LldQlCBQpM5LJ62QMBE/ZKLBikRHicShcPM3Et+lAaPAcI9HHUdStPAy+w==');

var_dump( $serviceResult );
