<?php

use RKFL\Api\Client\Options;
use RKFL\Api\Client\Rocketfuel;



function handlePost()
{

    if (isset($_POST['amount']) && isset($_POST['cart'])) {
        if (is_null($_POST['amount']) || is_null($_POST['cart'])) {
            return null;
        }
        require_once dirname(__DIR__,2) . '/../vendor/autoload.php';

        $payload = [
            'amount' => (string)$_POST['amount'],
            'cart' => json_decode($_POST['cart']),
            'currency' => 'USD',
            'order' => (string) time(),
            'redirectUrl' => $_POST['redirect']
        ];
        $read = file_get_contents(dirname(__DIR__) . '/options.dev_copy.json');
        $config = json_decode($read,true);

        $options = new Options($config);
        $sdk = new Rocketfuel($options);

        $serviceResult = $sdk->service()->getUUID($payload);
 
        return handleResult($serviceResult);
    } else {
        return null;
    }
}
function dnd($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}
function handleResult($serviceResult)
{
    if (!is_null($serviceResult)) {



        if (isset($serviceResult['ok']) && $serviceResult['ok'] === true) {

            header('Location: ' . $serviceResult['result']['url']);
            exit();
        } else {
            return $serviceResult;
        }
    }
}
