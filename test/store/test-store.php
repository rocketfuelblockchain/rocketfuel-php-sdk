<?php

require_once './helper.php';

$products  = [
    'aaa' =>
    [
        'id' => 'aaa',
        'price' => 5,
        'name' => 'Amazon Card 1',
        'image' => 'https://demoweed.com/wp-content/uploads/2021/07/ezgif-7-40f3ab0d901b-300x180.jpg'
    ],
    'bbb' => [
        'id' => 'bbb',
        'price' => 15,
        'name' => 'Amazon Card 2',
        'image' => 'https://demoweed.com/wp-content/uploads/2021/07/ezgif-7-40f3ab0d901b-300x180.jpg'

    ],
    'ccc' => [
        'id' => 'ccc',
        'price' => 10,
        'name' => 'Amazon Card 3',
        'type' => 'subscription',
        'subscriptionId' => 'ccc-sub',
        'interval' => 'daily',
        'image' => 'https://demoweed.com/wp-content/uploads/2021/07/ezgif-7-40f3ab0d901b-300x180.jpg'

    ]
];

$serviceResult = handlePost();

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Test Store for PHP SDK</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="asset/sdk-style.css">

</head>

<body>
    <div class="overlay">
        <div style="position: relative;height:100%">
            <div class="loader center"></div>
        </div>

    </div>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">

            <img class="logo" src="https://store.rocketfuelblockchain.com/wp-content/uploads/2021/04/RocketFuelLogo.png " alt="Rocketfuel">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">

                </ul>

            </div>
        </nav>
    </header>
    <main class="container" style="padding-top: 3rem;">
        <div class="alert alert-warning" role="alert" style="display: none;">

        </div>
        <section <?= isset($_GET['order']) ? 'style="display:none"' : ""; ?>>
            <div class="row">
                <div class="col-md-8 col-xs-12 col-sm-12">
                    <div>
                        <h2>
                            Product List
                        </h2>
                    </div>
                    <div class="card-group">
                        <?php
                        foreach ($products as $key => $product) {
                        ?>

                            <div class="card">
                                <img src="<?= $product['image']; ?>" class="card-img-top" alt="https://demoweed.com/wp-content/uploads/2021/07/ezgif-7-40f3ab0d901b-300x180.jpg">
                                <div class="card-body">
                                    <h5 class="card-title"><?= $product['name']; ?></h5>
                                    <p class="card-text">$ <?= $product['price']; ?></p>

                                    <div class="btn btn-primary cart" href="#" role="button" data-id="<?= $product['id']; ?>" data-name="<?= $product['name']; ?>" data-price="<?= $product['price']; ?>">add to cart</div>
                                </div>
                            </div>
                        <?php
                        }

                        ?>

                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                    <div class="card">
                        <div class="card-body cart-card">
                            <h5 class="card-title">Cart</h5>
                            <!-- <div class="cart-head">
                                <div>src</div>
                                <div class="product-name">Name</div>
                                <div>Quantity</div>
                                <div>Subtotal</div>
                            </div>
                            <div class="cart-items">

                            </div>
                            <div class="cart-totals">

                            </div> -->
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Quantity</th>
                                            <th scope="col">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody class="cart-items">

                                    </tbody>
                                    <tfoot>

                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <div class="cart-total"></div>
                                            </td>

                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                        <div class="card-body">
                            <form method="POST" id="rkfl-form">
                                <!-- <div class="form-group">
                                    <label for="exampleFormControlInput1">Name</label>
                                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="John Doe">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Email address</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput2" placeholder="name@example.com">
                                </div> -->
                                <input type="hidden" name="cart">
                                <input type="hidden" name="redirect" value="">
                                <input type="hidden" name="amount">
                                <button class="btn btn-primary" id="rkfl-submit" type="submit">Place Order</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <section <?= !isset($_GET['order']) ? 'style="display:none"' : ""; ?>>

            <div class="card pdt">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4" style="text-align: center;">Order Has been Placed</h1>

                        <div class="text-center">
                            <a href="test-store.php">Go to store</a>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </main><template>
        <tr class="cart-item">
            <!-- <th scope="row">1</th> -->
            <td>
                <div class="product-image"><img src="" alt=""></div>
            </td>
            <td>
                <span class="product-name">Name</span>
            </td>
            <td>
                <span class="qty">x 1</span>
            </td>
            <td>
                <span class="sub-total">x 1</span>
            </td>
            <td>
                <div>
                    <span class="delete">❌</span>
                </div>
            </td>
        </tr>
        <!-- <div class="cart-item">
        <div class="product-image"><img src="" alt=""></div>

        <p class="product-name">Name</p>
        <p class="qty">x 1</p>
        <p class="sub-total">x 1</p>

    </div> -->
    </template>
    <script>
        (() => {

            const products = <?= json_encode($products); ?>;

            const localstorageCartKey = 'rkfl-php-sdk-cart';

            const Helpers = {
                selector(selector) {
                    return document.querySelector(selector);
                },
                selectorAll(selector) {
                    return document.querySelectorAll(selector);
                },
                addToCart(productId, quantity) {

                    let cart = Helpers.getCart();

                    if (cart) {

                        if (cart[productId]) {
                            cart[productId].quantity += quantity;
                        } else {
                            cart[productId] = {
                                quantity
                            }
                        }


                    } else {
                        cart = {
                            [productId]: {
                                quantity
                            }
                        }
                    }
                    Helpers.setCart(cart);

                },
                getCart() {
                    let cart = localStorage.getItem(localstorageCartKey);
                    if (!cart) return null;
                    return JSON.parse(cart);

                },
                formSubmit(cart) {
                    if (cart) {
                        Helpers.selector('button#rkfl-submit').disabled = false;
                    } else {
                        Helpers.selector('button#rkfl-submit').disabled = true;

                    }
                },
                setCart(cart) {
                    localStorage.setItem(localstorageCartKey, JSON.stringify(cart));
                },
                removeFromCart(productId) {
                    const cart = Helpers.getCart();

                    delete cart[productId];
                    Helpers.setCart(cart);

                    Helpers.renderCart();
                },
                showError(message) {
                    Helpers.selector('.alert-warning').style.display = 'inherit';
                    Helpers.selector('.alert-warning').innerText = message || 'There is an error';
                    setTimeout(() => {
                        Helpers.selector('.alert-warning').style.display = 'none';

                    }, 2000)

                },
                renderCart() {

                    const cart = Helpers.getCart();
                    const template = document.getElementsByTagName("template")[0];
                    const cartIitem = template.content.querySelector('.cart-item');
                    const cartItems = document.querySelector('.cart-items');

                    cartItems.innerHTML = '';
                    let cartTotal = 0;
                    let parsedCart = [];

                    for (productId in cart) {
                        var clonedCartItem = cartIitem.cloneNode(true);

                        let item = cart[productId];
                        let subtotal = Number(products[productId].price) * Number(item.quantity)

                        clonedCartItem.querySelector('.product-name').innerText = products[productId].name;

                        clonedCartItem.querySelector('.product-image img').src = products[productId].image;

                        clonedCartItem.querySelector('.qty').innerText = `${products[productId].price} x ${item.quantity}`;

                        clonedCartItem.querySelector('.sub-total').innerText = subtotal;
                        clonedCartItem.querySelector('.delete').dataset.productId = productId;

                        clonedCartItem.querySelector('.delete').addEventListener('click', (e) => {

                            Helpers.removeFromCart(e.target.dataset.productId)

                        })
                        parsedCart.push({
                            id: productId,
                            name: products[productId].name,
                            price: products[productId].price,
                            quantity: item.quantity,
                        })
                        //  'id' => '1',
                        //             'name' => 'test',
                        //             'price' => '100',
                        //             'quantity' => '1'
                        cartItems.appendChild(clonedCartItem);
                        cartTotal += subtotal

                    }

                    document.querySelector('.cart-total').innerText = '$ ' + cartTotal;
                    document.querySelector('input[name=amount]').value = cartTotal;
                    document.querySelector('input[name=cart]').value = JSON.stringify(parsedCart)

                    Helpers.formSubmit(cart);

                },
                isCartEmpty(cart) {
                    for (var i in cart) return false;

                    return true
                },
                init() {
                    console.log(new URLSearchParams(window.location.search).get('order'));

                    if (new URLSearchParams(window.location.search).get('order')) {

                        console.log("localStorage.removeItem(localstorageCartKey)", localStorage.removeItem(localstorageCartKey));

                        localStorage.removeItem(localstorageCartKey);

                    }

                    document.addEventListener('DOMContentLoaded', () => {
                        Helpers.selector('form#rkfl-form').addEventListener('submit', (e) => {
                            e.preventDefault();

                            Helpers.selector('input[name=redirect]').value = window.location.href + '?order=true';

                            const cart = Helpers.getCart();

                            if (!cart || Helpers.isCartEmpty(cart)) {

                                Helpers.showError('No item in cart');

                            } else {
                                e.target.submit();
                                Helpers.selector('.overlay').style.display = 'inherit';
                                // Helpers.selector('main').style.cssText = 'filter: blur(1px)'

                            }

                        });

                        Helpers.renderCart();
                    })
                }
            }

            Helpers.selectorAll('.cart').forEach(element => {

                element.addEventListener('click', () => {

                    Helpers.addToCart(element.dataset.id, 1);
                    Helpers.renderCart();

                })

            });
            Helpers.init();
        })()
    </script>

</body>

</html>